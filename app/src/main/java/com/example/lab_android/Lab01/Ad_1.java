package com.example.lab_android.Lab01;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Ad_1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RelativeLayout layout = new RelativeLayout(this);
        final EditText EditText01 = new EditText(this);
        Button btn = new Button(this);


        EditText01.setHint("Nhập họ tên...");

        RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                android.widget.RelativeLayout.LayoutParams.WRAP_CONTENT);

        rlp.addRule(RelativeLayout.ALIGN_LEFT, EditText01.getId());
        rlp.addRule(RelativeLayout.LEFT_OF, btn.getId());
        EditText01.setLayoutParams(rlp);
        layout.addView(EditText01);


        btn.setText("Xin chào!");
        RelativeLayout.LayoutParams rlp2 = new RelativeLayout.LayoutParams(
                android.widget.RelativeLayout.LayoutParams.WRAP_CONTENT,
                android.widget.RelativeLayout.LayoutParams.WRAP_CONTENT);

        rlp2.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, btn.getId());
        btn.setLayoutParams(rlp2);
        layout.addView(btn);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg;
                msg = "Xin chào " + EditText01.getText().toString();
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            }
        });
        setContentView(layout);


    }


}
