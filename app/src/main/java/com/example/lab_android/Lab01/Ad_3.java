package com.example.lab_android.Lab01;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.lab_android.R;

public class Ad_3 extends AppCompatActivity {
    EditText ed1;
    EditText ed2;
    Spinner spn;
    Button btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ad_3);
        ed1 = findViewById(R.id.EditText01);
        ed2 = findViewById(R.id.EditText02);
        btn = findViewById(R.id.btn);
        spn = findViewById(R.id.spinner1);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float result = 0;
                int num1 = Integer.parseInt(ed1.getText().toString());
                int num2 = Integer.parseInt(ed2.getText().toString());
                String cal = spn.getSelectedItem().toString();
                try {
                    switch (cal) {
                        case "Cộng": {
                            result = num1 + num2;
                            break;
                        }
                        case "Trừ": {
                            result = num1 - num2;
                            break;
                        }
                        case "Nhân": {
                            result = num1 * num2;
                            break;
                        }
                        case "Chia": {
                            result = (float)num1 / num2;
                            break;
                        }

                    }
                }catch (Exception e) {
                    
                }
                String msg;
                msg = "" + String.valueOf(result).replace("Infinity","Không chia được");
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            }
        });


    }

}
