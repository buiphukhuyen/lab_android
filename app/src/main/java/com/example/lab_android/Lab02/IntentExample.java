package com.example.lab_android.Lab02;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.*;
import android.widget.*;
import android.content.*;
import com.example.lab_android.R;

public class IntentExample extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent_example);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        // lấy menu ngữ cảnh của ứng dụng
        MenuInflater inflater = getMenuInflater();
        // thiết lập menu
        inflater.inflate(R.menu.menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.clear:
                AlertDialog.Builder message = new AlertDialog.Builder(this); message.setTitle(R.string.clear_shortcut);
                message.setMessage(R.string.clear_shortcut);
                message.setNeutralButton(R.string.clear_shortcut, new
                    DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            EditText et = (EditText)findViewById(R.id.inputnote);
                            et.setText("");
                        } }).show();
                break;
            case R.id.setting:
                Intent intent = new Intent(this,OptionColors.class);
                final int result=1;
                // khởi tạo activity có lấy kết quả về
                startActivityForResult(intent, result);
                break;

            case R.id.exit:
                new AlertDialog.Builder(this) .setTitle(R.string.exit_shortcut) .setMessage(R.string.exit_content)
                        .setNegativeButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) { //thoát khỏi ứng dụng
                                finish();
                            }
                            })
                            .setPositiveButton(R.string.no, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    return;
                                }
                            })
                            .show();
                break;

        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // lấy Bundle dữ liệu
        Bundle bundle = data.getExtras();
        int index1 = bundle.getInt("ForeColor");
        int index2 = bundle.getInt("BackColor");
        int index3 = bundle.getInt("Resize");
        int size = bundle.getInt("InputResize");
        // lấy mảng màu
        String colorArray[] = getResources().getStringArray(R.array.color_array);
        // tham chiếu đến editText
        String sizeArray[] = getResources().getStringArray(R.array.resize_array);
        EditText et = (EditText)findViewById(R.id.inputnote);
        // thiết lập màu
        et.setTextColor(Color.parseColor(colorArray[index1]));
        et.setBackgroundColor(Color.parseColor(colorArray[index2]));
        // thiết lập kích thước
        et.setTextSize(Float.parseFloat(sizeArray[index3]));
        et.setTextSize(size);
    }
}
