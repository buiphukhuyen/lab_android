package com.example.lab_android.Lab02;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.*;
import android.view.*;
import com.example.lab_android.R;
import android.content.*;

import static android.widget.AdapterView.*;

public class OptionColors extends Activity {
    private int index1=0, index2=0, index3=0, fontsize = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_option_colors);
        Spinner spinner1 = (Spinner) findViewById(R.id.spinner1);
        spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                index1 = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        Spinner spinner2 = (Spinner)findViewById(R.id.spinner2);
        spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                index2 = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        Spinner spinner3 = (Spinner)findViewById(R.id.spinner3);
        spinner3.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                index3 = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



    }

    public void onOK(View view) {
        // Gởi dữ liệu về activity trước
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putInt("ForeColor", index1); // Lấy giá trị màu text
        bundle.putInt("BackColor", index2); // Lấy giá trị màu nền
        bundle.putInt("Resize", index3); // Lấy giá trị kích thước
        EditText editText = (EditText)findViewById(R.id.EditTextSize);
        fontsize = Integer.valueOf(editText.getText().toString());
        bundle.putInt("InputResize", fontsize);
        intent.putExtras(bundle); // Gởi kèm dữ liệu
        setResult(RESULT_OK, intent); // Gởi kết quả về
        finish(); // đóng activity
    }

}
