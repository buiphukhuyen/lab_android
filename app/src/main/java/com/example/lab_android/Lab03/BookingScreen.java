package com.example.lab_android.Lab03;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.lab_android.R;

public class BookingScreen extends AppCompatActivity {
    EditText editTextName;
    EditText editTextPhone;
    Button buttonOK;
    String Name = "";
    String Phone = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_screen);
        editTextName = findViewById(R.id.editTextName);
        editTextPhone = findViewById(R.id.editTextPhone);

        buttonOK = findViewById(R.id.btnOK);


        buttonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Name = editTextName.getText().toString();
                Phone = editTextPhone.getText().toString();
                Intent intent = new Intent(BookingScreen.this, DetailScreen.class);
                Bundle bundle = new Bundle();
                bundle.putString("Name", Name);
                bundle.putString("Phone", Phone);
                intent.putExtra("DATA", bundle);
                startActivityForResult(intent, 0);

            }
        });
    }

}
