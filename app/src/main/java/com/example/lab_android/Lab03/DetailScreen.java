package com.example.lab_android.Lab03;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.example.lab_android.R;

public class DetailScreen extends AppCompatActivity {
    TextView textViewName;
    TextView textViewPhone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_screen);
        textViewName = findViewById(R.id.TextViewName);
        textViewPhone = findViewById(R.id.TextViewPhone);
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("DATA");
        String Name = bundle.getString("Name");
        String Phone = bundle.getString("Phone");
        textViewName.setText(Name);
        textViewPhone.setText(Phone);
    }

}
