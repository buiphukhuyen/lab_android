package com.example.lab_android.Lab04_08;

import android.app.TabActivity;
import android.content.Context;
import android.os.Bundle;
import android.view.*;
import android.widget.*;
import android.database.Cursor;

import com.example.lab_android.R;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("ALL")
public class LunchList extends TabActivity {
    //private List<Restaurant> restaurantList = new ArrayList<Restaurant>();

    Cursor curRestaurant = null;
    RestaurantAdapter adapter = null;
    RestaurantHelper helper = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lunch_list);
        // khoi tao doi duong RestaurantHelper
        helper = new RestaurantHelper(this);
        Button save = findViewById(R.id.save);

        save.setOnClickListener(onSave);

        ListView list = (ListView)findViewById(R.id.restaurants);
        list.setOnItemClickListener(onListClick);

        /*adapter = new RestaurantAdapter();

        list.setAdapter(adapter);

        // Phần bổ sung cho Tab
        TabHost.TabSpec spec = getTabHost().newTabSpec("tag1");
        spec.setContent(R.id.restaurants);
        spec.setIndicator("List",getResources().getDrawable(R.drawable.list));
        getTabHost().addTab(spec);
        spec = getTabHost().newTabSpec("tag2");
        spec.setContent(R.id.details);
        spec.setIndicator("Details", getResources().getDrawable(R.drawable.restaurant));
        getTabHost().addTab(spec);
        getTabHost().setCurrentTab(0); */

        curRestaurant = helper.getAll();
        startManagingCursor(curRestaurant);
        adapter = new RestaurantAdapter(curRestaurant);
        list.setAdapter(adapter);
        TabHost.TabSpec spec = getTabHost().newTabSpec("tag1");
        spec.setContent(R.id.restaurants);
        spec.setIndicator("List",getResources().getDrawable(R.drawable.list));
        getTabHost().addTab(spec);
        spec = getTabHost().newTabSpec("tag2");
        spec.setContent(R.id.details);
        spec.setIndicator("Details",
                getResources().getDrawable(R.drawable.restaurant));
        getTabHost().addTab(spec);
        getTabHost().setCurrentTab(0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Đóng cơ sở dữ liệu
        helper.close();
    }// end onDestroy


    private View.OnClickListener onSave = new View.OnClickListener() {
        public void onClick(View v) {
            Restaurant r = new Restaurant();

            EditText name = (EditText) findViewById(R.id.name);
            EditText address = (EditText) findViewById(R.id.addr);

            r.setName(name.getText().toString());
            r.setAddress(address.getText().toString());
            RadioGroup type = (RadioGroup) findViewById(R.id.type);
            switch (type.getCheckedRadioButtonId()) {
                case R.id.take_out:
                    r.setType("Take out");
                    break;
                case R.id.sit_down:
                    r.setType("Sit down");
                    break;
                case R.id.delivery:
                    r.setType("Delivery");
                    break;
            }
            //restaurantList.add(r);
            // them vao CSDL
            helper.insert(r.getName(), r.getAddress(), r.getType());
            // refesh lai du lieu
            curRestaurant.requery();
        }
    };

    private AdapterView.OnItemClickListener onListClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            //Restaurant r = restaurantList.get(position); // lấy item được chọn
            curRestaurant.moveToPosition(position);
            EditText name;
            EditText address;
            RadioGroup types;
            // Tham chiếu đến các view trong details
            name = findViewById(R.id.name);
            address = findViewById(R.id.addr);
            types = findViewById(R.id.type);

           /* // thiết lập thông tin tương ứng
            name.setText(r.getName());
            address.setText(r.getAddress());
            if (r.getType().equals("Sit down"))
                types.check(R.id.sit_down);
            else if (r.getType().equals("Take out"))
                types.check(R.id.take_out);
            else
                types.check(R.id.delivery);
                // sinh viên có thể bổ sung lệnh sau để chuyển view về tab details
                getTabHost().setCurrentTab(1); */

            name.setText(helper.getName(curRestaurant));
            address.setText(helper.getAddress(curRestaurant));
            if (helper.getType(curRestaurant).equals("Sit down"))
                types.check(R.id.sit_down);
            else if (helper.getType(curRestaurant).equals("Take out"))
                types.check(R.id.take_out);
            else
                types.check(R.id.delivery);
            // chuyen qua tab detail
            getTabHost().setCurrentTab(1);
        }
    };

  /*  class RestaurantAdapter extends ArrayAdapter<Restaurant> {

        public RestaurantAdapter(Context context, int textViewResoureId) {
            super(context, textViewResoureId);
        }

        public RestaurantAdapter(Cursor curRestaurant) {
            super(LunchList.this, android.R.layout.simple_list_item_1, restaurantList);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if(row==null) {
                LayoutInflater inflater = getLayoutInflater();
                row = inflater.inflate(R.layout.row, null);
            }
            Restaurant r = restaurantList.get(position);
            System.out.println(r.getName());
            System.out.println(r.getAddress());
            ((TextView)row.findViewById(R.id.title)).setText(r.getName());
            ((TextView)row.findViewById(R.id.address)).setText(r.getAddress());
            ImageView icon = (ImageView)row.findViewById(R.id.icon);

            String type = r.getType();
            if(type.equals("Take out"))
                icon.setImageResource(R.drawable.icon_t);
            else if(type.equals("Sit down"))
                icon.setImageResource(R.drawable.icon_s);
            else
                icon.setImageResource(R.drawable.icon_d);
            return row;
        }
    }*/

    class RestaurantAdapter extends CursorAdapter {

        RestaurantAdapter(Cursor c) {
            super(LunchList.this, c);
        }

        public RestaurantAdapter(Context context, Cursor c) {
            super(context, c);
        }


        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {

            LayoutInflater inflater = getLayoutInflater();
            View row = inflater.inflate(R.layout.row, parent, false);
            return row;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            View row = view;
            ((TextView)row.findViewById(R.id.title)).setText(helper.getName(cursor));
            ((TextView)row.findViewById(R.id.address)).setText(helper.getAddress(cursor));
            ImageView icon = (ImageView)row.findViewById(R.id.icon);
            String type = helper.getType(cursor);
            if (type.equals("Take out"))
                icon.setImageResource(R.drawable.icon_t);
            else if (type.equals("Sit down"))
                icon.setImageResource(R.drawable.icon_s);
            else
                icon.setImageResource(R.drawable.icon_d);
        }

    }
}



